# Flotorizer

## Executive Summary
Flotorizer allows timestamping into the FLO blockchain creating Proof of Existence documentation and interaction with the Open Index Protocol.

### Purpose
The reason and purpose is very broad as in reflection to the potential utilities  

### Key contributors
Davi Ortega

Pankaj S Y

Anthony Howl

## What
The amount of ways PoE can be utilized is boundless but here are a few examples

- Demonstration of data ownership without revealing actual data

- Proving ownership and Deed transfer

- Asset & Commodity realization

- Checks and balances for document integrity 

Duplication of stored items is not possible without some form of alteration. This allows vallidation of truth

## Competition

    Factom:  Pricing? Breakdown?
    Proof of Existence: Pricing? Breakdown?
    OriginStamp: Pricing? Breakdown? 

## Pricing

    PPTS - Pay Per Timestamp & Enterprise Solutions

    Subscription Based &
    Enterprise Solutions
## RoadMap

## Funding rounds
CrowdFund

Series A

Series B

Series C

## Conclusion

## Reference
